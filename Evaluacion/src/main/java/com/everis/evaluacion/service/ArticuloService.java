package com.everis.evaluacion.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

import com.everis.evaluacion.entity.Articulo;
import com.everis.evaluacion.repository.ArticuloRepository;

@Service
public class ArticuloService implements BaseService<Articulo> {
	private ArticuloRepository articuloRepository;
	
	public ArticuloService(ArticuloRepository articuloRepository) {
		this.articuloRepository = articuloRepository;
	}
	
	@Override
	@Transactional
	public List<Articulo> findAll() throws Exception {
		try {
			List<Articulo> entities = articuloRepository.findAll();
			return entities;

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	@Transactional
	public Articulo findById(Long id) throws Exception {
		Optional<Articulo> entitySingle = articuloRepository.findById(id);
		System.out.println(entitySingle.get());
		return entitySingle.get();
	}
	
	@Override
	@Transactional
	public Articulo save(Articulo entity) throws Exception {
		try {		
			entity = articuloRepository.save(entity);
			return entity;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	@Transactional
	public Articulo update(Long id, Articulo entity) throws Exception {
		try {
			Optional<Articulo> entityOptional = articuloRepository.findById(id);
			Articulo articulo = entityOptional.get();
			articulo = articuloRepository.save(entity);
			return articulo;

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	@Override
	@Transactional
	public boolean delete(Long id) throws Exception {
		try {
			if (articuloRepository.existsById(id)) {
				articuloRepository.deleteById(id);
				return true;
			} else {
				throw new Exception();
			}
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	
	@Override
	@Transactional
	public List<Articulo> findActivos() throws Exception {
		try {
			List<Articulo> entities = (List<Articulo>) articuloRepository.findAll();
			List<Articulo> entitiesRetorno = entities.stream().filter(p->p.getEstArticulo() == 1).collect(Collectors.toList());
			return entitiesRetorno;

		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}

	
}
