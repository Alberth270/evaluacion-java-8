package com.everis.evaluacion.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder

public class ArticuloDTO {
	private Long codarticulo;
	private String nomArticulo;
	private Long codcategory;
	private int precArticulo;
	private int estArticulo;
}
