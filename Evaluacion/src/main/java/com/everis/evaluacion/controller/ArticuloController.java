package com.everis.evaluacion.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.evaluacion.dto.ArticuloDTO;
import com.everis.evaluacion.entity.Articulo;
import com.everis.evaluacion.entity.Category;
import com.everis.evaluacion.service.ArticuloService;


@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "api/articulo")
public class ArticuloController {
	private ArticuloService articuloService;
	public ArticuloController(ArticuloService articuloService) {
		this.articuloService = articuloService;
	}
	@GetMapping("")
	public ResponseEntity<?> getAll() {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(articuloService.findAll());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@GetMapping("/{id}")
	public ResponseEntity<?> getOne(@PathVariable Long id) {
		try {
			System.out.println("id:"+ id);
			return ResponseEntity.status(HttpStatus.OK).body(articuloService.findById(id));

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@PostMapping("")
	public ResponseEntity<?> save(@RequestBody ArticuloDTO entity) {
		System.out.println(entity);
		try {
			Articulo articulo = new Articulo();
			Category category = new Category();
			category.setCodcategory(entity.getCodcategory());
			articulo.setCodarticulo(entity.getCodarticulo());
			articulo.setNomArticulo(entity.getNomArticulo());
			articulo.setPrecArticulo(entity.getPrecArticulo());
			articulo.setEstArticulo(entity.getEstArticulo());
			articulo.setCategory(category);
			return ResponseEntity.status(HttpStatus.OK).body(articuloService.save(articulo));
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody ArticuloDTO entity) {
		try {
			Articulo articulo = new Articulo();
			Category category = new Category();
			category.setCodcategory(entity.getCodcategory());
			articulo.setCodarticulo(entity.getCodarticulo());
			articulo.setNomArticulo(entity.getNomArticulo());
			articulo.setPrecArticulo(entity.getPrecArticulo());
			articulo.setEstArticulo(entity.getEstArticulo());
			articulo.setCategory(category);
			return ResponseEntity.status(HttpStatus.OK).body(articuloService.update(id, articulo));

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(articuloService.delete(id));

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@GetMapping("/activos")
	public ResponseEntity<?> getActivos() {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(articuloService.findActivos());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
}
