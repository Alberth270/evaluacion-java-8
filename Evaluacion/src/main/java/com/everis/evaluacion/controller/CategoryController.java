package com.everis.evaluacion.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.everis.evaluacion.entity.Category;
import com.everis.evaluacion.service.CategoryService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "api/category")
public class CategoryController {
	private CategoryService categoryService;
	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	@GetMapping("")
	public ResponseEntity<?> getAll() {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(categoryService.findAll());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@GetMapping("/{id}")
	public ResponseEntity<?> getOne(@PathVariable Long id) {
		try {
			System.out.println("id:"+ id);
			return ResponseEntity.status(HttpStatus.OK).body(categoryService.findById(id));

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@PostMapping("")
	public ResponseEntity<?> save(@RequestBody Category entity) {
		try {
			
			System.out.println(entity);
			
			return ResponseEntity.status(HttpStatus.OK).body(categoryService.save(entity));

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@PutMapping("/{id}")
	public ResponseEntity<?> update(@PathVariable Long id, @RequestBody Category entity) {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(categoryService.update(id, entity));

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id) {
		try {
			
			return ResponseEntity.status(HttpStatus.NO_CONTENT).body(categoryService.delete(id));

		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
	@GetMapping("/activos")
	public ResponseEntity<?> getActivos() {
		try {
			return ResponseEntity.status(HttpStatus.OK).body(categoryService.findActivos());
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND)
					.body("{\"error\":\"Error. Por favor intente mas tarde.\"}");
		}
	}
}
