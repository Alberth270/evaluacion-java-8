package com.everis.evaluacion.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.everis.evaluacion.entity.Articulo;

@Repository
public interface ArticuloRepository extends JpaRepository<Articulo, Long>{
}
